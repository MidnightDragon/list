


/* 

*/

// creating li using input field and button
let form = document.querySelector('form');
let input = document.querySelector('input');
let listArrP = document.querySelector('.listArr');

let listArr = [];

let Item = function (itemID, itemValue) {
    this.itemID = itemID;
    this.itemValue = itemValue;
}

let li, ul, button, index, itemID, itemValue, newItem;
let maxID = 0;


// dom variables
var DOMstrings = {
    listGroupItem: 'list-group-item',
    deleteBtn: 'delete-button',
    deleteBtnClass: 'btn btn-outline-danger float-right delete-button',
};



// save li to array as object
function saveToArr(itemID, itemValue) {
    newItem = new Item(itemID, itemValue);

    // add input to array
    listArr.push(newItem);
    // console.log(listArr);
}


// create li
form.addEventListener('submit', createLi);

// create element function
function createLi(e) {
    // select ul element, store it in a variable
    ul = document.querySelector('ul');

    // create li item
    li = document.createElement('li');

    // add class
    li.className = DOMstrings.listGroupItem;

    // add text content to li
    li.textContent = `${input.value} `;

    // =========================================
    // create button element to delete li
    button = document.createElement('button');

    // add class
    button.className = DOMstrings.deleteBtnClass;

    // add text
    button.textContent = 'Delete';

    
    // append button to li
    li.appendChild(button);

    // append li as a child to ul
    ul.appendChild(li);


    if(listArr.length === 0 && maxID === 0) {
        li.id = maxID;
        itemID = parseInt(li.id);
        itemValue = input.value;
    }else{  
        // add id to li
        maxID++;
        for (let i = 0; i < maxID; i++) { 
            
            li.id = maxID;
                
            itemID = parseInt(li.id);
            itemValue = input.value;   
        } 

    }

    saveToArr(itemID, itemValue);
  
    // clear input
    input.value = '';
}


// ==================================================
// delete item
document.body.addEventListener('click', deleteItem);

// delete from array
function delFromArr(e) {  
    indexInLi = parseInt(e.target.parentElement.id);

    listArr.forEach(function(newItem) {
        if(newItem.itemID === indexInLi) {     
            index = listArr.findIndex(x => x.itemID === indexInLi);
                
            // delete item from array
            listArr.splice(index, 1);   
        } else { 
            // console.log('no match');
        }
    });

    /* 
        index = listArr.findIndex(x => x.itemID === 2);
        this is good shit, finds index of object property
        in array
    */
}

// create function to delete item
function deleteItem(e) {   
    if (e.target.classList.contains(DOMstrings.deleteBtn)) {
        e.target.parentElement.remove();
        delFromArr(e);
    }
}













